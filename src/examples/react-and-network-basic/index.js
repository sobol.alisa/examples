/* Core */
import { render } from 'react-dom';

/* Instruments */
import Example from './5';

render(<Example />, document.getElementById('root'));
